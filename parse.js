const __parse = (ast) => {
    switch (ast.type) {
        case 'ArrayExpression': {
            return ast.elements.map(__parse)
        }
        case 'ObjectExpression': {
            return createObjectFromAST(ast.properties)
        }
        case 'NullLiteral': {
            return null;
        }
        default: {
            return ast.value;
        }
    }
}

const createObjectFromAST = (properties) => {
    return Object.fromEntries(properties.map((property) => [property.key.value,__parse(property.value)]))
}

module.exports = {
    __parse,
    createObjectFromAST,
}