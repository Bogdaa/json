const babel = require('@babel/parser');
const traverse = require('@babel/traverse').default;
const { createObjectFromAST } = require('./parse');

const __stringify = (value) => {
    switch (typeof value) {
        case "string": {
            return `"${value}"`
        }
        case "object": {
            if(value === null) {
                return 'null';
            }
            if(value instanceof Array) {
                return `[${value.map(__stringify).join(',')}]`
            }
            return CustomJSON.stringify(value)
        }
        case "function": {
            return 'null';
        }
        default: {
            return `${value}`;
        }
    }
}



class CustomJSON {
    static stringify(object) {
        const parsed = Object.entries(object).map(([key, value]) => `"${key}":${__stringify(value)}`)
        return `{${parsed}}`
    }

    static parse(json) {
        const ast = babel.parse(`let a = ${json}`);
        let object = null;
        traverse(ast, {
            VariableDeclarator:({ node }) => {
                object = createObjectFromAST(node.init.properties)
            }
        })
        return object;
    }
}

const object = {
    name: 'andrew',
    address: {
        streetAddress: '21st street',
        city: 'New York',
        state: 'NY'
    },
    array: ['one', 2, false, null, { value: 'five', or: 2 }]
}

const json = CustomJSON.stringify(object)

console.log(CustomJSON.parse(json))
